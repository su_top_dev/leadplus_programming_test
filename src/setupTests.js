// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
// import "@testing-library/jest-dom/extend-expect";
import "@testing-library/jest-dom";

import { server } from "./mocks/handler";

beforeAll(() => {
  server.listen();
  // we're using fake timers because we don't want to
  // wait a full second for this test to run.
  // jest.useFakeTimers();
});
afterEach(() => server.resetHandlers());
afterAll(() => {
  // jest.useRealTimers();
  server.close();
});
