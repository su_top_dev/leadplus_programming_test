export const base_api_url = "https://financialmodelingprep.com/api/v3";
export const all_companies = ["AAPL", "GOOGL", "AMZN"];

//Error message
//It might be used/itegrated with i18n later.
export const error_msg = {
  number_required: "A number is required!",
  positive_required: "This field should be positive!",
  less_than_1000: "It should be less than 1000!",
  totalPrice_invalid: "Total Price should be less than $1,000,000 (USD)!",
  open_time_invalid: "Stock Opens Monday to Friday, 09:00-11:30, 12:30-15:00!",
};
