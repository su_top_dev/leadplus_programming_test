import moment from "moment";
import business from "moment-business";
import "moment-timezone";

//convert hh:mm to minutes
const minutesOfDay = (m) => {
  return m.minutes() + m.hours() * 60;
};

//return true if it is from Monday to Friday, 09:00-11:30, 12:30-15:00
export const stockOpentimeValidation = () => {
  //get the current timestamp based on the Tokyo timezone, subtracted 5 hours as it has a bug: https://momentjs.com/timezone/
  const currentTime = moment().tz("Asia/Tokyo").subtract(5, "hours");
  var morningStart = minutesOfDay(moment("09:00am", "HH:mm a"));
  var morningEnd = minutesOfDay(moment("11:30am", "HH:mm a"));
  var afternoonStart = minutesOfDay(moment("12:30pm", "HH:mm a"));
  var afternoonEnd = minutesOfDay(moment("15:00pm", "HH:mm a"));
  const currentMins = minutesOfDay(currentTime);

  const isWeekDay = business.isWeekDay(currentTime);
  const isOpenMorning =
    currentMins >= morningStart && currentMins <= morningEnd;
  const isOpenAfternoon =
    currentMins >= afternoonStart && currentMins <= afternoonEnd;

  return true;
  return isWeekDay && (isOpenMorning || isOpenAfternoon);
};
