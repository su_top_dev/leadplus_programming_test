import { base_api_url } from "../utils/constants";

export const getHistoricalDailyPrice = async (company, from, to) => {
  try {
    const url = `${base_api_url}/historical-price-full/${company}?from=${from}&to=${to}&apikey=${process.env.REACT_APP_API_KEY}`;

    const response = await fetch(url);

    if (response.status >= 400) {
      const message = await response.text();
      throw new Error(message);
    }

    return response.json();
  } catch (e) {
    throw e;
  }
};

export const getLatestClostPrice = async (company) => {
  try {
    const url = `${base_api_url}/quote-short/${company}?apikey=${process.env.REACT_APP_API_KEY}`;

    const response = await fetch(url);

    if (response.status >= 400) {
      const message = await response.text();
      throw new Error(message);
    }

    return response.json();
  } catch (e) {
    throw e;
  }
};
