import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import cartReducer from "./reducers/cart";
import appReducer from "./reducers/app";
import { createLogger } from "redux-logger";
const middleware = [...getDefaultMiddleware()];

if (process.env.NODE_ENV === "development") {
  const logger = createLogger({
    level: "info",
    collapsed: true,
  });
  middleware.push(logger);
}

export const store = configureStore({
  reducer: {
    cart: cartReducer,
    app: appReducer,
  },
  middleware,
});
