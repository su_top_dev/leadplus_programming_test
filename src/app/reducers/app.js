import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoading: false,
  notification: {
    type: null,
    message: null,
    description: null,
  },
};

export const appSlice = createSlice({
  name: "app",
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    setNotification: (state, action) => {
      state.notification = action.payload;
    },
    resetNotification: (state, action) => {
      state.notification = null;
    },
    startLoading: (state, action) => {
      state.isLoading = true;
    },
    endLoading: (state, action) => {
      state.isLoading = false;
    },
  },
});

export const { setNotification, resetNotification, startLoading, endLoading } =
  appSlice.actions;

export default appSlice.reducer;
