import cartReducer, { addCart, removeCart, editCart } from "./cart";

describe("counter reducer", () => {
  const initialState = {
    data: [
      {
        id: 111,
        price: 111,
        company: "AAPL",
        purchaseAmount: 70,
      },
    ],
  };
  it("should handle initial state", () => {
    const actual = cartReducer(undefined, { type: "unknown" });
    expect(actual.data).toHaveLength(0);
  });

  it("should handle addCart", () => {
    const actual = cartReducer(
      initialState,
      addCart({
        id: 123,
        price: 34,
        company: "GOOGL",
        purchaseAmount: 30,
      })
    );
    expect(actual.data).toHaveLength(2);
    expect(actual.data[1]).toMatchObject({
      id: 123,
      price: 34,
      company: "GOOGL",
      purchaseAmount: 30,
    });
  });

  it("should handle removeCart", () => {
    const actual = cartReducer(initialState, removeCart(111));
    expect(actual.data).toHaveLength(0);
  });

  it("should handle editCart", () => {
    const newOne = {
      id: 111,
      price: 111,
      company: "GOOGL",
      purchaseAmount: 456,
    };
    const actual = cartReducer(initialState, editCart(newOne));
    expect(actual.data[0]).toMatchObject(newOne);
  });
});
