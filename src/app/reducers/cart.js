import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { act } from "react-dom/test-utils";
import uniqid from "uniqid";

const initialState = {
  data: [
    // {
    //   id: 123,
    //   price: 34,
    //   company: 'GOOGL',
    //   purchaseAmount: 30
    // },
  ],
};

export const cartSlice = createSlice({
  name: "cart",
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    addCart: (state, action) => {
      // Redux Toolkit allows us to write "mutating" logic in reducers. It
      // doesn't actually mutate the state because it uses the Immer library,
      // which detects changes to a "draft state" and produces a brand new
      // immutable state based off those changes
      state.data.push({ id: uniqid(), ...action.payload });
    },
    removeCart: (state, action) => {
      const newOne = state.data.filter((item) => item.id !== action.payload);
      state.data = newOne;
    },
    editCart: (state, action) => {
      const cartItem = state.data.find((item) => item.id === action.payload.id);
      if (cartItem) {
        cartItem.purchaseAmount = action.payload.purchaseAmount;
        cartItem.company = action.payload.company;
      }
    },
  },
});

export const { addCart, removeCart, editCart } = cartSlice.actions;

export const getCartItemWithId = (id) => (dispatch, getState) => {
  const store = getState();
  return store.cart.data.filter((item) => id === item.id)[0];
};
export default cartSlice.reducer;
