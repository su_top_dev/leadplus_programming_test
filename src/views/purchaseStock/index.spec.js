import React from "react";
import {
  act,
  render,
  screen,
  fireEvent,
  waitFor,
} from "@testing-library/react";
import { Provider } from "react-redux";
import { store } from "../../app/store";
import PurchaseStock from "../purchaseStock";
import { BrowserRouter as Router } from "react-router-dom";
import userEvent from "@testing-library/user-event";
import { error_msg } from "../../utils/constants";
describe("App", () => {
  beforeEach(async () => {
    await act(async () =>
      render(
        <Provider store={store}>
          <Router>
            <PurchaseStock />
          </Router>
        </Provider>
      )
    );
  });

  it("should display required error when value is invalid", async () => {
    await fireEvent.click(
      screen.getByRole("button", {
        name: /add to cart/i,
      })
    );
    const items = await screen.findAllByText(error_msg.number_required);
    expect(items).toHaveLength(1);
  });
  it("should display number required error msg when value is not number", async () => {
    fireEvent.change(screen.getByTestId("purchaseAmount-input"), {
      target: { value: "notNumber" },
    });

    await fireEvent.click(
      screen.getByRole("button", {
        name: /add to cart/i,
      })
    );
    const items = await screen.findAllByText(error_msg.number_required);
    expect(items).toHaveLength(1);
  });
  it("should display number required error msg when value is greater than 1000", async () => {
    fireEvent.change(screen.getByTestId("purchaseAmount-input"), {
      target: { value: 10001 },
    });
    await fireEvent.click(
      screen.getByRole("button", {
        name: /add to cart/i,
      })
    );
    const items = await screen.findAllByText(error_msg.less_than_1000);
    expect(items).toHaveLength(1);
  });
  it("should display number required error msg when value is greater than 1000", async () => {
    //As we are using selector from AntD, should simulate selecting action like human does.

    const select = screen.getByTestId("select-company").firstElementChild;
    await userEvent.click(select);
    // await waitFor(() => expect(screen.getByTitle("GOOGL")).toBeInTheDocument());
    await screen.findByTitle("GOOGL");
    const item = screen.getByTitle("GOOGL");
    // await waitFor(() => fireEvent.click(item));
    await fireEvent.click(item);
    //wait until it fetches latest price
    await screen.findByText(/2930.96/i);

    fireEvent.input(screen.getByTestId("purchaseAmount-input"), {
      target: {
        value: 700,
      },
    });
    await fireEvent.click(
      screen.getByRole("button", {
        name: /add to cart/i,
      })
    );

    const items = await screen.findAllByText(error_msg.totalPrice_invalid);
    expect(items).toHaveLength(1);
  });
});
