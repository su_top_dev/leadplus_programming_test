import { useEffect, useMemo, useState, useRef } from "react";
import { useParams, useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { useForm } from "react-hook-form";
import { Select } from "antd";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import styled from "styled-components";
import PropTypes from "prop-types";
import { ShoppingCartOutlined } from "@ant-design/icons";

import { getLatestClostPrice } from "../../service/apis";
import { all_companies, error_msg } from "../../utils/constants";
import { addCart, editCart, getCartItemWithId } from "../../app/reducers/cart";
import { stockOpentimeValidation } from "../../utils/tools";
import { setNotification } from "../../app/reducers/app";
import { StyledLink, StyledSelect } from "../../components/StyledComponents";

const { Option } = Select;

const StyledInput = styled.input`
  border-radius: 5px;
  height: 33px;
  width: 250px;
  display: block;
  margin: auto;
  margin-top: 20px;
  border: 1px solid #d9d9d9;
  padding: 0 10px;
`;
const AddCart = styled.button`
  // border-radius: 5px;
  height: 33px;
  width: 250px;
  display: block;
  margin: auto;
  margin-top: 20px;
  color: #1890ff;
  border-color: #1890ff;
  cursor: pointer;
  background: white;
  border: 1px solid #40a9ff;
  border-radius: 5px;
`;
const Error = styled.p`
  color: red;
`;

const ButtonWrapper = styled.div`
  width: 250px;
  display: flex;
  justify-content: space-between;
  margin: auto;
  margin-top: 20px;
  align-items: center;
  flex-direction: column;
  a {
    margin-bottom: 20px;
  }
`;

const PurchaseStock = ({ editable }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [price, setPrice] = useState(0);
  const [totalPrice, setTotalPrice] = useState(0);
  const [company, setCompany] = useState(all_companies[0]);
  const { id } = useParams();
  const inputRef = useRef(null);
  //create the validator for purchase form written by rect-hook-form
  const validationSchema = useMemo(() => {
    return yup
      .object({
        purchaseAmount: yup
          .number()
          .positive(error_msg.positive_required)
          .integer()
          .max(1000, error_msg.less_than_1000)
          .required()
          .typeError(error_msg.number_required)
          .test(
            "check total price",
            error_msg.totalPrice_invalid,
            function (item) {
              return item * price < 1000000;
            }
          )
          //it might be better option to disable submit button until market opens rather than validate here.
          .test(
            "check market open time",
            error_msg.open_time_invalid,
            stockOpentimeValidation
          ),
      })
      .required();
  }, [price]);

  const resolver = yupResolver(validationSchema);

  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
    reset,
  } = useForm({ resolver });

  const { ref, ...rest } = register("purchaseAmount");

  useEffect(() => {
    //setCompany and latest Price, and total Price, and input value for amount
    if (id) {
      const cartItem = dispatch(getCartItemWithId(id));
      if (cartItem) {
        setCompany(cartItem.company);
        setTotalPrice(cartItem.price * cartItem.purchaseAmount);
        reset({ purchaseAmount: cartItem.purchaseAmount });
      }
    }
  }, [id, reset]);

  useEffect(() => {
    if (inputRef.current) {
      inputRef.current.focus();
    }
  }, []);

  //handle when submit
  const onSubmit = (data) => {
    if (!editable) {
      //when add new purchase to cart
      dispatch(
        addCart({
          price: price,
          company,
          purchaseAmount: data.purchaseAmount,
        })
      );
      dispatch(
        setNotification({
          type: "success",
          message: "Success",
          description: "Successfully Added",
        })
      );
    } else {
      //when edit the cart
      if (!id) {
        //throw error: something went wrong using toast
      }
      dispatch(
        editCart({
          id,
          price: price,
          company,
          purchaseAmount: data.purchaseAmount,
        })
      );
      history.push("/cart");
    }
  };

  //need to get latest close price
  useEffect(() => {
    const fetchData = async () => {
      try {
        const data = await getLatestClostPrice(company);
        if (data[0] && data[0].price) {
          setPrice(data[0].price);
        }
      } catch (e) {
        dispatch(
          setNotification({
            type: "error",
            message: "Error",
            description: "Something wrong internally, try it again later!",
          })
        );
      }
    };
    fetchData();
  }, [company]);

  // calculate the total price by provided amount and latest price
  useEffect(() => {
    const subscription = watch((value, { purchaseAmount }) =>
      setTotalPrice(price * value.purchaseAmount)
    );
    return () => subscription.unsubscribe();
  }, [watch, price]);

  return (
    <div>
      <form onSubmit={handleSubmit(onSubmit)}>
        <StyledSelect
          value={company}
          style={{ width: 250 }}
          onChange={setCompany}
          data-testid="select-company"
          virtual={false}
        >
          {all_companies.map((item, index) => (
            <Option value={item} key={item + index}>
              {item}
            </Option>
          ))}
        </StyledSelect>
        <h2>Latest Price: ${price}</h2>
        <StyledInput
          // {...register("purchaseAmount")}
          {...rest}
          name="purchaseAmount"
          placeholder="Amount"
          data-testid="purchaseAmount-input"
          ref={(e) => {
            ref(e);
            inputRef.current = e; // you can still assign to ref
          }}
        />
        <Error>{errors.purchaseAmount?.message}</Error>
        <h2>Total Price: ${Math.round(totalPrice * 100) / 100}</h2>
        <AddCart type="submit" value="">
          {!editable ? (
            <>
              <ShoppingCartOutlined style={{ maringRight: 30 }} />
              <span>Add to Cart</span>
            </>
          ) : (
            "Save"
          )}
        </AddCart>
      </form>
      <ButtonWrapper>
        <StyledLink to={`/cart`}>
          {editable ? "Cancel" : "Go to payment"}
        </StyledLink>
        <StyledLink to={`/chart`}>Go to Home</StyledLink>
      </ButtonWrapper>
    </div>
  );
};

PurchaseStock.propTypes = {
  editable: PropTypes.bool,
};

PurchaseStock.defaultProps = {
  editable: false,
};

export default PurchaseStock;
