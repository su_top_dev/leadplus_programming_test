import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { Table, Space } from "antd";
import { DeleteFilled, EditFilled } from "@ant-design/icons";
import styled from "styled-components";

import { removeCart } from "../../app/reducers/cart";
import { StyledLink, StyledSelect } from "../../components/StyledComponents";

const ButtonWrapper = styled.div`
  width: 350px;
  display: flex;
  justify-content: space-between;
  margin: auto;
  margin-top: 20px;
  a {
    margin-right: 30px;
  }
`;

const CartList = () => {
  const allPurchaseItems = useSelector((state) => state.cart.data);
  const dispatch = useDispatch();
  const history = useHistory();

  //define columns to be used in the antd table to customize.
  const columns = [
    {
      title: "Company",
      dataIndex: "company",
      key: "company",
    },
    {
      title: "Amount",
      dataIndex: "purchaseAmount",
      key: "purchaseAmount",
    },
    {
      title: "Price",
      dataIndex: "price",
      key: "price",
      render: (price) => <>${price}</>,
    },
    {
      title: "Total Price",
      dataIndex: "price",
      key: "totalprice",
      render: (price, record) => <>${price * record.purchaseAmount}</>,
    },
    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <Space size="middle">
          <DeleteFilled onClick={() => dispatch(removeCart(record.id))}>
            Remove
          </DeleteFilled>
          <EditFilled onClick={() => history.push(`/cart/edit/${record.id}`)}>
            Edit
          </EditFilled>
        </Space>
      ),
    },
  ];

  return (
    <div>
      <h2>Cart</h2>
      <Table
        columns={columns}
        dataSource={allPurchaseItems}
        style={{ width: "70%", margin: "auto" }}
      />
      <ButtonWrapper>
        <StyledLink to={`/chart`}>Go to Home</StyledLink>
        <StyledLink to="/purchase">Add more</StyledLink>
      </ButtonWrapper>
    </div>
  );
};

export default CartList;
