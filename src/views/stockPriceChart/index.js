import { useEffect, useMemo, useState } from "react";
import { DatePicker, Select } from "antd";
import styled from "styled-components";
import { Chart } from "react-google-charts";
import moment from "moment";
import { all_companies } from "../../utils/constants";
import { getHistoricalDailyPrice } from "../../service/apis";
import { useDispatch } from "react-redux";
import {
  endLoading,
  setNotification,
  startLoading,
} from "../../app/reducers/app";
import { StyledLink, StyledSelect } from "../../components/StyledComponents";

const { RangePicker } = DatePicker;
const { Option } = Select;

const ChartContainer = styled.div`
  display: flex;
  justify-content: center;
`;

const StyledDatePicker = styled(RangePicker)`
  border-radius: 5px;
  margin: 0 15px;
`;

const StockPriceChart = () => {
  const dispatch = useDispatch();
  const [company, setCompany] = useState(all_companies[0]);
  const [chartData, setChartData] = useState(null);
  //it is showing data for the previous 50 days as default.
  const [dateRange, setDateRange] = useState({
    from: moment().subtract(50, "d").format("YYYY-MM-DD"),
    to: moment().format("YYYY-MM-DD"),
  });

  const chart_options = useMemo(() => {
    return {
      chart: {
        title: `${company} Close Price`,
        subtitle: `${dateRange.from} - ${dateRange.to}`,
      },
    };
  }, [company, dateRange]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        dispatch(startLoading());
        const data = await getHistoricalDailyPrice(
          company,
          dateRange.from,
          dateRange.to
        );

        if (data && data.historical) {
          const _data = data.historical.map((item) => [item.date, item.close]);
          setChartData([["Date", "Closing Price"], ..._data]);
        } else {
          setChartData(null);
        }
        dispatch(endLoading());
      } catch (e) {
        dispatch(
          setNotification({
            type: "error",
            message: "Error",
            description: "Something wrong internally, try it again later!",
          })
        );
        dispatch(endLoading());
      }
    };
    fetchData();
  }, [company, dateRange]);

  const handleSelect = (data) => {
    setCompany(data);
  };

  const handleDateRangeChange = (date, dateString) => {
    setDateRange({
      from: dateString[0],
      to: dateString[1],
    });
  };

  return (
    <div>
      <StyledSelect
        value={company}
        style={{ width: 120 }}
        onChange={handleSelect}
      >
        {all_companies.map((item, index) => (
          <Option value={item} key={item + index}>
            {item}
          </Option>
        ))}
      </StyledSelect>
      <StyledDatePicker
        onChange={handleDateRangeChange}
        defaultValue={[moment(dateRange.from), moment(dateRange.to)]}
      />
      <StyledLink to={`/purchase`}>Purchase</StyledLink>
      <ChartContainer>
        {chartData && (
          <Chart
            chartType="Bar"
            width="70%"
            height="500px"
            data={chartData}
            options={chart_options}
          />
        )}
      </ChartContainer>
    </div>
  );
};
export default StockPriceChart;
