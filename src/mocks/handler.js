// src/mocks/handlers.js
import { rest } from "msw";
import { setupServer } from "msw/node";

const mockData = {
  AAPL: {
    symbol: "AAPL",
    price: 171.83,
    volume: 86460270,
  },
  GOOGL: {
    symbol: "GOOGL",
    price: 2930.96,
    volume: 1554389,
  },
  AMZN: {
    symbol: "AMZN",
    price: 3175.12,
    volume: 3837408,
  },
};

export const server = setupServer(
  rest.get(
    "https://financialmodelingprep.com/api/v3/quote-short/:company",
    (req, res, ctx) => {
      const { company } = req.params;
      return res(ctx.json([mockData[company]]));
    }
  )
);
