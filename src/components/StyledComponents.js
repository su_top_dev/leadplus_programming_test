import { Select } from "antd";
import { Link } from "react-router-dom";
import styled from "styled-components";

export const StyledLink = styled(Link)`
  text-decoration: none;
  position: relative;
  border: 1px solid #40a9ff;
  border-radius: 4px;
  padding: 5px 10px;
  width: 100%;
`;

export const StyledSelect = styled(Select)`
  &&& .ant-select-selector {
    border-radius: 5px;
  }
`;
