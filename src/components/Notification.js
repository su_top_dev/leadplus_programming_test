import { notification } from "antd";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { resetNotification } from "../app/reducers/app";

//handle all notification like toast message in the application, it will watch the notification variable in redux store
const NotificationCenter = () => {
  const dispatch = useDispatch();
  const _notification = useSelector((state) => state.app.notification);

  const openNotificationWithIcon = (data) => {
    notification[data.type]({
      message: data.message,
      description: data.description,
    });
  };

  useEffect(() => {
    if (_notification && _notification.type) {
      openNotificationWithIcon(_notification);
      dispatch(resetNotification());
    }
  }, [_notification]);
  return null;
};

export default NotificationCenter;
