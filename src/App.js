import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { useSelector } from "react-redux";
import { Spin } from "antd";
import { LoadingOutlined } from "@ant-design/icons";
import styled from "styled-components";

import StockPriceChart from "./views/stockPriceChart";
import PurchaseStock from "./views/purchaseStock";
import CartList from "./views/cart";
import NotificationCenter from "./components/Notification";
import "./App.css";

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

const AppContainer = styled.div`
  margin: 100px;
`;
const Header = styled.h1`
  margin-bottom: 50px;
  margin-top: 100px;
`;

function App() {
  const isLoading = useSelector((state) => state.app.isLoading);
  return (
    <AppContainer className="App">
      <Header>React Test Assignment</Header>
      <Router>
        <Switch>
          <Route path="/chart">
            <StockPriceChart />
          </Route>
          <Route path="/purchase">
            <PurchaseStock />
          </Route>
          <Route path="/cart/edit/:id">
            <PurchaseStock editable />
          </Route>
          <Route path="/cart">
            <CartList />
          </Route>
          <Redirect to="/chart" />
        </Switch>
      </Router>
      <Spin
        indicator={antIcon}
        spinning={isLoading}
        style={{ marginTop: 200 }}
      />
      <NotificationCenter />
    </AppContainer>
  );
}

export default App;
